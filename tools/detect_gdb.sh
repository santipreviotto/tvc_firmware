#!/bin/bash
for NUMBER in 0 1 2 3 4 5
do
    # check if exists
    {
        ls /dev/ttyACM${NUMBER};
    }>/dev/null 2>&1
    if [[ $? -eq 0 ]]
    then
        # check if responds to gdb command within a 1 second timeout
        {
            timeout -s SIGKILL 1 gdb-multiarch -nx --batch -ex 'target extended-remote /dev/ttyACM'"${NUMBER}";
        }>/dev/null 2>&1
        if [[ $? -eq 0 ]]
        then
            echo $NUMBER
        fi
    fi
done
