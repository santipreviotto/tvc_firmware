/* Copyright (C) 2024  Santiago Previotto
 * \file selftest_bldc.h
 */
#ifndef SRC_SELFTEST_SELFTEST_BLDC_H_
#define SRC_SELFTEST_SELFTEST_BLDC_H_
#include <lib/common/retval.h>
#include <pico/stdlib.h>

/**
 *  \brief  Self test to check bldc initialization
 *          function.
 */
retval_t self_test_bldc_init();

/**
 *  \brief  Self test to check bldc deinitialization
 *          function.
 */
retval_t self_test_bldc_deinit();

/**
 *  \brief  Self test to check bldc set angle
 *          function.
 */ 
retval_t self_test_bldc_set(float angle);

#endif  // SRC_SELFTEST_SELFTEST_BLDC_H_
