/* Copyright (C) 2024  Santiago Previotto
 * \file selftest_bldc.c
 */
#include <lib/actuator/bldc.h>
#include <selftest/selftest_bldc.h>

#define ROLL_PHASE_U           6        /**< \brief  Output for phase u. */
#define ROLL_PHASE_V           7        /**< \brief  Output for phase v. */
#define ROLL_PHASE_W           10       /**< \brief  Output for phase w. */
#define ROLL_PHASE_U_CHANNEL   0        /**< \brief  Channel of timer assigned for U phase of roll. */
#define ROLL_PHASE_V_CHANNEL   1        /**< \brief  Channel of timer assigned for V phase of roll. */
#define ROLL_PHASE_W_CHANNEL   0        /**< \brief  Channel of timer assigned for W phase of roll. */
#define ROLL_PHASE_U_ENABLE    13       /**< \brief  Output for u enable pin. */
#define ROLL_PHASE_V_ENABLE    14       /**< \brief  Output for v enable pin. */
#define ROLL_PHASE_W_ENABLE    17       /**< \brief  Output for w enable pin. */
#define ROLL_SLEEP             18       /**< \brief  Output for sleep pin. */
#define ROLL_RESET             16       /**< \brief  Output for reset pin. */
#define ROLL_FREQUENCY         15000    /**< \brief  Frequency of roll actuator. */
#define PERIOD_MS              100      /**< \brief Period for task in milliseconds. */
#define PERIOD                 \
pdMS_TO_TICKS(PERIOD_MS)                /**< \brief Period for task in ticks. */
#define FALSE                  0        /**< \brief Zero value. */

/**< \brief Define sentence for bldc actuator. */
static DEFINE_BLDC(bldc_roll, ROLL_PHASE_U,
                   ROLL_PHASE_V, ROLL_PHASE_W,
                   ROLL_PHASE_U_CHANNEL, ROLL_PHASE_V_CHANNEL,
                   ROLL_PHASE_W_CHANNEL, ROLL_PHASE_U_ENABLE,
                   ROLL_PHASE_V_ENABLE, ROLL_PHASE_W_ENABLE,
                   ROLL_SLEEP, ROLL_RESET, ROLL_FREQUENCY);

retval_t self_test_bldc_init() {
    bldc_init(&bldc_roll);
    return RV_SUCCESS;
}

retval_t self_test_bldc_deinit() {
    bldc_deinit(&bldc_roll);
    return RV_SUCCESS;
}

retval_t self_test_bldc_set(float angle) {
    bldc_set(&bldc_roll, angle);
    return RV_SUCCESS;
}
