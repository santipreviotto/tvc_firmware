cmake_minimum_required(VERSION 3.13)
include(pico_sdk_import.cmake)
include(FreeRTOS_Kernel_import.cmake)

project(main C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)
set(PICO_BOARD pico_w)

pico_sdk_init()

include_directories(config/freertos)
add_subdirectory(app)
add_subdirectory(lib)
add_subdirectory(selftest)
