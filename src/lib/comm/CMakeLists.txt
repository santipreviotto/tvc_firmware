add_library(uart
    src/uart.c
)

target_link_libraries(uart
    pico_stdlib         # core built in sdk
    pico_stdio_uart     # uart built in sdk
    pico_stdio_usb      # usb built in sdk
)

target_include_directories(
    uart
    PUBLIC
    ../..
)
