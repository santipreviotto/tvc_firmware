/* Copyright (C) 2024  Santiago Previotto
 * \file uart.h
 */
#ifndef SRC_LIB_COMM_UART_H_
#define SRC_LIB_COMM_UART_H_
#include <lib/common/retval.h>
#include <pico/stdlib.h>

typedef struct {
    uint8_t id;
    uint32_t baud_rate;
    uint8_t tx_pin;
    uint8_t rx_pin;
} uart_config_t;

typedef struct uart_s uart_t;

typedef retval_t (uart_init_t)(uart_t *uart);
typedef char (uart_read_t)();
typedef retval_t (uart_write_t)(char *buf);

struct uart_s {
    uart_config_t config;
    uart_init_t *init;
    uart_read_t *read;
    uart_write_t *write;
};

/**
 *  \brief  Initialize uart communication
 *          by configuring name, baud rate,
 *          transmitter and receiver pins.
 *  \param  uart: pointer to uart struct
 */
retval_t comm_uart_init(uart_t *uart);

/**
 *  \brief  Perform a uart read and return
 *          a character.
 */
char comm_uart_read();

/**
 *  \brief  Perform a uart write with the
 *          buffer character.
 *  \param  buf: pointer to buffer char
 */
retval_t comm_uart_write(char *buf);

#define DECLARE_UART_CONFIG(__id, __baud_rate,          \
                            __tx_pin, __rx_pin)         \
    {                                                   \
        .id = __id,                                     \
        .baud_rate = __baud_rate,                       \
        .tx_pin = __tx_pin,                             \
        .rx_pin = __rx_pin,                             \
    }

#define DEFINE_UART(_name, _id, _baud_rate,             \
                    _tx_pin, _rx_pin)                   \
    uart_t _name = {                                    \
        .config = DECLARE_UART_CONFIG(                  \
        _id, _baud_rate, _tx_pin, _rx_pin),             \
        .init = comm_uart_init,                         \
        .read = comm_uart_read,                         \
        .write = comm_uart_write,                       \
    }

#endif  // SRC_LIB_COMM_UART_H_
