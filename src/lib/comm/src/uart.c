/* Copyright (C) 2024  Santiago Previotto
 * \file uart.c
 */
#include <stdio.h>
#include <pico/stdlib.h>
#include <lib/comm/uart.h>

retval_t comm_uart_init(uart_t *uart) {
    stdio_usb_init();
    switch (uart->config.id) {
        case 0:
            stdio_uart_init_full(uart0, uart->config.baud_rate, uart->config.tx_pin, uart->config.rx_pin);
            break;
        case 1:
            stdio_uart_init_full(uart1, uart->config.baud_rate, uart->config.tx_pin, uart->config.rx_pin);
            break;
        default:
            return RV_ERROR;
    }
    if (stdio_usb_connected() == 0) return RV_ERROR;
    return RV_SUCCESS;
}

char comm_uart_read() {
    char buf;
    if (stdio_usb_connected()) {
        buf = getchar();
        stdio_flush();
    }
    return buf;
}

retval_t comm_uart_write(char *buf) {
    if (stdio_usb_connected()) {
        printf("%s\n", buf);
    } else {
        return RV_ERROR;
    }
    return RV_SUCCESS;
}
