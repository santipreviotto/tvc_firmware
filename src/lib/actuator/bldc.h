/* Copyright (C) 2023  Santiago Previotto
 * \file bldc.h
 */
#ifndef SRC_LIB_ACTUATOR_BLDC_H_
#define SRC_LIB_ACTUATOR_BLDC_H_
#include <lib/common/retval.h>
#include <pico/stdlib.h>

typedef struct {
    uint8_t out_u; uint8_t out_v;
    uint8_t out_w; uint8_t ch_u;
    uint8_t ch_v; uint8_t ch_w;
    uint8_t en_u; uint8_t en_v;
    uint8_t en_w; uint8_t sleep;
    uint8_t reset; uint16_t freq;
} bldc_config_t;

typedef struct bldc_s bldc_t;

typedef retval_t (bldc_init_f)(bldc_t *bldc);
typedef retval_t (bldc_deinit_f)(bldc_t *bldc);
typedef retval_t (bldc_set_f)(bldc_t *bldc,
                 float angle);

struct bldc_s {
    bldc_config_t config;
    bldc_init_f *init;
    bldc_deinit_f *deinit;
    bldc_set_f *set;
    float angle;
};

/**
 *  \brief  Initialize the brushless actuator by
 *          configuring pwm and pins for control.
 *  \param  bldc: pointer to brushless struct
 */
retval_t bldc_init(bldc_t *bldc);

/**
 *  \brief  Deinitialize the brushless actuator
 *          by power off pwm and pins for
 *          control.
 *  \param  bldc: pointer to brushless struct
 */
retval_t bldc_deinit(bldc_t *bldc);

/**
 *  \brief  Calculates the prescaler that should
 *          be applied to the pwm timer according
 *          to the freq.
 *  \param  freq: freq in hz
 */
retval_t bldc_set(bldc_t *bldc, float angle);

#define DECLARE_BLDC_CONFIG(__out_u, __out_v,   \
                            __out_w, __ch_u,    \
                            __ch_v,  __ch_w,    \
                            __en_u,  __en_v,    \
                            __en_w,  __sleep,   \
                            __reset, __freq)    \
    {                                           \
        .out_u = __out_u, .out_v = __out_v,     \
        .out_w = __out_w, .ch_u = __ch_u,       \
        .ch_v = __ch_v, .ch_w = __ch_w,         \
        .en_u = __en_u, .en_v = __en_v,         \
        .en_w = __en_w, .sleep = __sleep,       \
        .reset = __reset, .freq = __freq,       \
    }

#define DEFINE_BLDC(_name, __out_u, __out_v,    \
                    __out_w, __ch_u, __ch_v,    \
                    __ch_w, __en_u, __en_v,     \
                    __en_w, __sleep, __reset,   \
                    __freq)                     \
    bldc_t _name = {                            \
        .config = DECLARE_BLDC_CONFIG(          \
        __out_u, __out_v, __out_w, __ch_u,      \
        __ch_v, __ch_w, __en_u, __en_v,         \
        __en_w, __sleep, __reset, __freq),      \
        .init = bldc_init,                      \
        .deinit = bldc_deinit,                  \
        .set = bldc_set,                        \
    }

#endif  // SRC_LIB_ACTUATOR_BLDC_H_
