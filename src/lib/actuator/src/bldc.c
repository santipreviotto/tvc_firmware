/* Copyright (C) 2023  Santiago Previotto
 * \file bldc.c
 */
#include <math.h>
#include <lib/actuator/bldc.h>
#include <hardware/pwm.h>

#define CPU_FREQ                    125000000   /**< \brief Microcontroller clock freq in hz. */
#define COUNT_MAX                   8192        /**< \brief Maximum count for pwm timer. */
#define COUNT_INIT                  0           /**< \brief Initialization count for pwm timer. */
#define PROP_FREQ                   \
((float)(CPU_FREQ) / (float)(COUNT_MAX))        /**< \brief Proportion to calcule pwm freq. */
#define PROP_DUTY                   \
((float)(COUNT_MAX) / 100.f)                    /**< \brief Proportion to calcule pwm freq. */
#define FREQ_MIN                    60          /**< \brief Minimum pwm freq. */
#define FREQ_MAX                    15257       /**< \brief Maximum pwm freq. */
#define DIV_MIN                     1.f         /**< \brief Minimum clock divider. */
#define DIV_MAX                     255.f       /**< \brief Maximum clock divider. */
#define AMP                         2           /**< \brief Amplitud of sine wave. */
#define GAP                         120         /**< \brief Gap between phases in degrees. */
#define OFFSET                      1           /**< \brief Offset of sine wave. */
#define RAD                         59.3        /**< \brief Equivalent to 1 degrees in radians. */
#define DUTY_MAX                    95          /**< \brief Maximum percentage for duty. */
#define MASK_NULL                   0x00000000  /**< \brief Mask to disable pwm timers. */
#define MASK_ALL                    0x000000FF  /**< \brief Mask to enable pwm timers. */
#define ANGLE_MIN                   0.f         /**< \brief Minimum angle in degrees. */
#define ANGLE_MAX                   360.f       /**< \brief Maximum angle in degrees. */

/**
 *  \brief  Calculates the prescaler that should be applied to
 *          the pwm timer according to the freq.
 *  \param  freq: freq in hz
 */
static float prescaler_calcule(uint16_t freq) {
    if (freq < FREQ_MIN) return DIV_MAX;
    if (freq > FREQ_MAX) return DIV_MIN;
    return (PROP_FREQ / (float)(freq));
}

/**
 *  \brief  Calculates the amount of duty according to the
 *          indicated grades for phase u.
 *  \param  angle: angle in degrees
 */
static float angle_calcule_u(float angle) {
    float value;
    float angle_value_rad;
    angle_value_rad = angle / RAD;
    value = sin(angle_value_rad) + (float)(OFFSET);
    return (value * ((float)(DUTY_MAX) / (float)(AMP)));
}

/**
 *  \brief  Calculates the amount of duty according to the
 *          indicated grades for phase v.
 *  \param  angle: angle in degrees
 */
static float angle_calcule_v(float angle) {
    float value;
    float angle_value_rad;
    angle_value_rad = (angle - (float)(GAP)) / RAD;
    value = sin(angle_value_rad) + (float)(OFFSET);
    return (value * ((float)(DUTY_MAX) / (float)(AMP)));
}

/**
 *  \brief  Calculates the amount of duty according to the
 *          indicated grades for phase w.
 *  \param  angle: angle in degrees
 */
static float angle_calcule_w(float angle) {
    float value;
    float angle_value_rad;
    angle_value_rad = (angle + (float)(GAP)) / RAD;
    value = sin(angle_value_rad) + (float)(OFFSET);
    return (value * ((float)(DUTY_MAX) / (float)(AMP)));
}

retval_t bldc_init(bldc_t *bldc) {
    /* Get a set of default values for PWM configuration. */
    pwm_config cfg = pwm_get_default_config();
    /* Determine the PWM slice that is attached to the specified GPIO. */
    uint slice_u = pwm_gpio_to_slice_num(bldc->config.out_u);
    uint slice_v = pwm_gpio_to_slice_num(bldc->config.out_v);
    uint slice_w = pwm_gpio_to_slice_num(bldc->config.out_w);
    /* Set phase correction in a PWM configuration. */
    pwm_config_set_phase_correct(&cfg, true);
    /* Sets the clock division and maximum count. */
    pwm_set_clkdiv(slice_u, prescaler_calcule(bldc->config.freq));
    pwm_set_clkdiv(slice_v, prescaler_calcule(bldc->config.freq));
    pwm_set_clkdiv(slice_w, prescaler_calcule(bldc->config.freq));
    pwm_config_set_clkdiv_mode(&cfg, PWM_DIV_FREE_RUNNING);
    pwm_set_mask_enabled(MASK_NULL);
    pwm_set_counter(slice_u, COUNT_INIT);
    pwm_set_counter(slice_v, COUNT_INIT);
    pwm_set_counter(slice_w, COUNT_INIT);
    pwm_set_mask_enabled(MASK_ALL);
    pwm_set_wrap(slice_u, COUNT_MAX);
    pwm_set_wrap(slice_v, COUNT_MAX);
    pwm_set_wrap(slice_w, COUNT_MAX);
    /* Enable PWM outputs. */
    gpio_set_function(bldc->config.out_u, GPIO_FUNC_PWM);
    gpio_set_function(bldc->config.out_v, GPIO_FUNC_PWM);
    gpio_set_function(bldc->config.out_w, GPIO_FUNC_PWM);
    pwm_set_enabled(slice_u, true);
    pwm_set_enabled(slice_v, true);
    pwm_set_enabled(slice_w, true);
    /* Initialize control outputs. */
    gpio_init(bldc->config.en_u);
    gpio_init(bldc->config.en_v);
    gpio_init(bldc->config.en_w);
    gpio_init(bldc->config.sleep);
    gpio_init(bldc->config.reset);
    gpio_set_dir(bldc->config.en_u, GPIO_OUT);
    gpio_set_dir(bldc->config.en_v, GPIO_OUT);
    gpio_set_dir(bldc->config.en_w, GPIO_OUT);
    gpio_set_dir(bldc->config.sleep, GPIO_OUT);
    gpio_set_dir(bldc->config.reset, GPIO_OUT);
    /* Enable control outputs. */
    gpio_put(bldc->config.en_u, true);
    gpio_put(bldc->config.en_v, true);
    gpio_put(bldc->config.en_w, true);
    gpio_put(bldc->config.sleep, true);
    gpio_put(bldc->config.reset, true);
    return RV_SUCCESS;
}

retval_t bldc_deinit(bldc_t *bldc) {
    /* Determine the PWM slice that is attached to the specified GPIO. */
    uint slice_u = pwm_gpio_to_slice_num(bldc->config.out_u);
    uint slice_v = pwm_gpio_to_slice_num(bldc->config.out_v);
    uint slice_w = pwm_gpio_to_slice_num(bldc->config.out_w);
    /* Disable PWM outputs. */
    pwm_set_enabled(slice_u, false);
    pwm_set_enabled(slice_v, false);
    pwm_set_enabled(slice_w, false);
    /* Disable control outputs. */
    gpio_put(bldc->config.en_u, false);
    gpio_put(bldc->config.en_v, false);
    gpio_put(bldc->config.en_w, false);
    gpio_put(bldc->config.sleep, false);
    gpio_put(bldc->config.reset, false);
    return RV_SUCCESS;
}

retval_t bldc_set(bldc_t *bldc, float angle) {
    /* Filter angle value. */
    if (angle < ANGLE_MIN) angle = ANGLE_MIN;
    if (angle > ANGLE_MAX) angle = ANGLE_MAX;
    /* Determine the PWM slice that is attached to the specified GPIO. */
    uint slice_u = pwm_gpio_to_slice_num(bldc->config.out_u);
    uint slice_v = pwm_gpio_to_slice_num(bldc->config.out_v);
    uint slice_w = pwm_gpio_to_slice_num(bldc->config.out_w);
    /* Set PWM duty values. */
    pwm_set_chan_level(slice_u, bldc->config.ch_u,
    (angle_calcule_u(angle) * PROP_DUTY));
    pwm_set_chan_level(slice_v, bldc->config.ch_v,
    (angle_calcule_v(angle) * PROP_DUTY));
    pwm_set_chan_level(slice_w, bldc->config.ch_w,
    (angle_calcule_w(angle) * PROP_DUTY));
    return RV_SUCCESS;
}
