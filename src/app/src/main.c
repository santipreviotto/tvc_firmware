/* Copyright (C) 2024  Santiago Previotto
 * \file main.c
 */
#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <pico/stdlib.h>
#include <lib/comm/uart.h>
#include <selftest/selftest_bldc.h>

#define MSG_ROTATION_CW         '+'     /**< \brief Character for clockwise rotation. */
#define MSG_ROTATION_CCW        '-'     /**< \brief Character for counter clockwise rotation. */
#define N_ANGLE                 10      /**< \brief Size of angle array. */
#define N_ANGLE_MIN             1       /**< \brief Minimum angle rotation step. */
#define N_TICKS_TIMEOUT         10      /**< \brief Number of ticks for queue timeout. */
#define T_UART_PERIOD_MS        100     /**< \brief Period for uart task in milliseconds. */
#define T_UART_INIT_MS          10      /**< \brief Period for uart initialization in milliseconds. */
#define T_TEST_PERIOD_MS        1000    /**< \brief Period for test task in milliseconds. */
#define T_TEST_ITER_MS          100     /**< \brief Time in milliseconds for iteration. */
#define T_TEST_ITER             \
pdMS_TO_TICKS(T_TEST_ITER_MS)           /**< \brief Time in ticks for iteration. */
#define UART_BAUD_RATE          115200  /**< \brief Uart baud rate. */
#define UART_ID                 0       /**< \brief Uart identification module. */
#define UART_TX                 0       /**< \brief Uart transmitter pin. */
#define UART_RX                 1       /**< \brief Uart receiver pin. */

static DEFINE_UART(uart_over_usb, UART_ID, UART_BAUD_RATE, UART_TX, UART_RX);

QueueHandle_t xQueue1 = NULL;

void test_task(void *pvParameters) {
    char msg;
    TickType_t xPeriodicity = T_TEST_PERIOD_MS;
    TickType_t xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        if (xQueueReceive(xQueue1, &msg, (TickType_t)N_TICKS_TIMEOUT)) {
            if (msg == MSG_ROTATION_CW) {
                self_test_bldc_init();
                for (int iter_angle = 0; iter_angle < N_ANGLE; iter_angle++) {
                    self_test_bldc_set((float)(iter_angle * N_ANGLE_MIN));
                    vTaskDelay(T_TEST_ITER);
                }
                self_test_bldc_deinit();
            }
            if (msg == MSG_ROTATION_CCW) {
                self_test_bldc_init();
                for (int iter_angle = N_ANGLE; iter_angle >= 0; iter_angle--) {
                    self_test_bldc_set((float)(iter_angle * N_ANGLE_MIN));
                    vTaskDelay(T_TEST_ITER);
                }
                self_test_bldc_deinit();
            }
        }
        vTaskDelayUntil(&xLastWakeTime, xPeriodicity);
    }
}

void comm_task(void *pvParameters) {
    char msg;
    comm_uart_init(&uart_over_usb);
    comm_uart_write("uart is initialized!");
    TickType_t xPeriodicity = T_UART_PERIOD_MS;
    TickType_t xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        msg = comm_uart_read();
        if (msg == MSG_ROTATION_CW) comm_uart_write("test rotation cw");
        if (msg == MSG_ROTATION_CCW) comm_uart_write("test rotation ccw");
        xQueueSend(xQueue1, &msg, (TickType_t)N_TICKS_TIMEOUT);
        vTaskDelayUntil(&xLastWakeTime, xPeriodicity);
    }
}

retval_t main() {
    BaseType_t xReturned1;
    BaseType_t xReturned2;
    TaskHandle_t xHandle1 = NULL;
    TaskHandle_t xHandle2 = NULL;
    xReturned1 = xTaskCreate(&test_task,
                            "test_task",
                            configMINIMAL_STACK_SIZE,
                            NULL, tskIDLE_PRIORITY+2,
                            &xHandle1);
    xReturned2 = xTaskCreate(&comm_task,
                            "comm_task",
                            configMINIMAL_STACK_SIZE,
                            NULL, tskIDLE_PRIORITY+2,
                            &xHandle2);
    if (xReturned1 != pdPASS) return RV_ERROR;
    if (xReturned2 != pdPASS) return RV_ERROR;
    xQueue1 = xQueueCreate(1, sizeof(char));
    if (xQueue1 == NULL) return RV_ERROR;
    vTaskStartScheduler();
    while (1) {
    }
}
